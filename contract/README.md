# EOSPets contracts

`deploy.sh` will redeploy the contract into a locally-running docker image.
Check `runcommands.txt` for ways to populate the contract with some basic animals and organisations.
`schema.md` is out of date, so ignore it in favour of direct table dumps from the chain.
