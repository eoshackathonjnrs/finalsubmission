# here's where the schema shit is


### Animal repository.
Each animal (e.g. gorilla) will have a set of data associated with it. Each organisation will have their own instance of an animal, so a WWF Gorilla is not the same as a Greenpeace Gorilla

Each animal within this dictionary is of Animal type
```
{
animals:
    {
        animal1: {
            id: random-string?
            species: ratus ratus
            common-name: brown rat
            organisation_id: Association
        },
        
        animal2: {

        },
        ...
    }
}
```

### Associations
Wildlife organisations will be able to register onto the site and start deploying animals.

```
{
organisations: 
    {
        wwf: {
            id: pubkey?
            name: "World Wildlife Foundation"
        }

    }
}

```



### Users
The site will be comprised of users that will own animals, and be able to interact (feed, groom, etc.) with their own animals

```
{
users:
    {
        billybob123: {
            id: pubkey
            balance: ..
            owned_animals: [
                {
                    anim: Animal,
                    food-level: X,
                },    
                ...
            ]
        }
    }
```        
