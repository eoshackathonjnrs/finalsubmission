#include <eosiolib/eosio.hpp>

/*
    sik ass contract v0.69
                                                        __              
                                                       /  |             
     ______    ______    _______   ______    ______   _$$ |_    _______ 
    /      \  /      \  /       | /      \  /      \ / $$   |  /       |
   /$$$$$$  |/$$$$$$  |/$$$$$$$/ /$$$$$$  |/$$$$$$  |$$$$$$/  /$$$$$$$/ 
   $$    $$ |$$ |  $$ |$$      \ $$ |  $$ |$$    $$ |  $$ | __$$      \
   $$$$$$$$/ $$ \__$$ | $$$$$$  |$$ |__$$ |$$$$$$$$/   $$ |/  |$$$$$$  |
   $$       |$$    $$/ /     $$/ $$    $$/ $$       |  $$  $$//     $$/ 
    $$$$$$$/  $$$$$$/  $$$$$$$/  $$$$$$$/   $$$$$$$/    $$$$/ $$$$$$$/  
                                 $$ |                                   
                                 $$ |                                   
                                 $$/                                    
*/


#include <eosiolib/eosio.hpp>

using namespace eosio;

typedef uint64_t item_id;


class eospets : public contract {
    private:
        /** helper functions **/

        bool anim_owned_by(const uint64_t& animid, const account_name& owner);

        void assert_org(const account_name& org);
        void assert_item(const item_id& itemid);
        void assert_user(const account_name& username);
        void assert_anim(const uint64_t& animid);
        void assert_itemoffer(const uint64_t offerid);
        void assert_animoffer(const uint64_t offerid);

        bool is_orphan(const uint64_t animal_id);

        static uint16_t clamp(uint16_t val, uint16_t min, uint16_t max);
        static uint16_t compute_satisfaction(double happiness, double belly);
        static uint16_t compute_hunger(const uint32_t& last_fed,
                                const uint16_t& last_belly,
                                const uint16_t& decay_rate);

        /// maximum animal trait value (e.g. happiness)
        static constexpr uint16_t MAX_STAT = 1000;

    public:
        eospets(account_name self) :
            contract(self),
            _animals(self, self),
            _orgs(self, self),
            _itemoffers(self, self), 
            _animoffers(self, self), 
            _users(self, self),
            _items(self, self) {}
        
        // make organisation with account
        void makeorg(const account_name& o);
        // make a user with this name
        void makeuser(const account_name& u);

    
        /** organisation interactions **/

        // create an animal owned by an organisation
        // decay rate indicates the number of seconds for each level of hungry increase
        void makeanimal(const account_name& o, std::string species_name,
                        std::string common_name, uint16_t decay_rate);
        // make an item associated to an organisation, with a name,
        // a type (either "FOOD", "CONSUMABLE", or "ACCESSORY"), and deltas:
        // how much it changes happiness and hunger (+ decreases hunger)
        void makeitem(const account_name& orgo, const std::string& name,
                      std::string type, int16_t hap_delta, int16_t hung_delta);
        // put an item up on the marketplace (under organisation) with price
        void offeritem(const account_name& orgo, const item_id& itemid, 
                       const uint64_t& price);

        // put an offer for animals up on the marketplace
        void offeranim(const account_name& orgo, const std::string& species_name,
                       const std::string& common_name, const uint64_t price);
// user buying an offered animal    
        void buyanim(const account_name& owner, const uint64_t& animoffid);

        /** user interactions **/
        
        // user buying an item
        void buyitem(const account_name& buyer, const uint64_t offerid);

        // change an animal by id to a new name
        // the animal must not have a prior name (i.e. == "")
        void setanimname(const account_name& owner, const uint64_t& id, 
                         const std::string& new_name);
        // use an item by itemid on an animal by animalid
        void useitem(const account_name& owner, const item_id& itemid,
                     const uint64_t& animalid);

        // transfer ownership of an animal to another user (called by fromacc)
        void transfer(const account_name& fromacc, const account_name& toacc,
                      const uint64_t& animid);

        // send this animal to the pound (set ownership to eospets)
        void abandonanim(const account_name& owner, const uint64_t& animid);

        // update the state of the blockchain by animal id (calculated by time) 
        void update(const account_name& payer, const uint64_t& animal_id);


        void sendmoney(const account_name& payer, const account_name& receiver, const uint64_t& amount) {
            require_auth(payer);
            assert_user(payer);
            assert_user(receiver);

            auto payit = _users.find(payer);
            auto recit = _users.find(receiver);

            eosio_assert(payit->balance >= amount, "user does not have enough funds for this transaction");

            // take money from sender
            _users.modify(payit, payer, [&](auto& u) {
                u.balance -= amount;
                    });

            // give money to receiver
            _users.modify(recit, payer, [&](auto& u) {
                u.balance += amount;
                    });
        }



        /** some contracts are here just to aide debugging/setup **/

        void printitemid(const account_name& owner);

        // ! this would be private, but allows setting up of animal logs easily !
        // the log str is appended to the history of the animal
        void addlog(const account_name& o, const uint64_t animid, 
                    const std::string& log) ;

    public:
        /** table data **/

        // organisation - is stored within `orgs'
        /// @abi table orgs i64
        struct org {
            account_name owner;
            // most recent animal created by this organisation
            uint64_t mr_animal;
            // most recent item created
            uint64_t mr_item;

            auto primary_key() const { return owner; }

            EOSLIB_SERIALIZE( org, (owner)(mr_animal)(mr_item)) 
        };

        typedef eosio::multi_index<N(orgs), org> orgs;
        orgs _orgs;

        /* offers that are on the marketplace  - stored within `itemoffers' */
        /// @abi table itemoffers i64
        struct itemoffer {
            // offer id
            uint64_t id;
            // who put the offer on
            account_name org;

            item_id itemid;
            // units of currency that will be withdraw from the user's account
            uint64_t price;

            auto primary_key() const { return id; } 
            account_name by_org() const { return org; }

            EOSLIB_SERIALIZE( itemoffer, (id)(org)(itemid)(price))
        };

        typedef eosio::multi_index<N(itemoffers), itemoffer> itemoffers;
        itemoffers _itemoffers;

        /* offering animals on the marketplace  - stored within `animoffers' */
        /// @abi table animoffers i64
        struct animoffer {
            uint64_t id;
            account_name org;
            std::string species_name;
            std::string common_name;

            uint64_t price;
            uint64_t qty; // UNUSED

            auto primary_key() const { return id; }
            account_name by_org() const { return org; }

            EOSLIB_SERIALIZE( animoffer, (id)(org)(species_name)\
                    (common_name)(price)(qty))
        };
        typedef eosio::multi_index<N(animoffers), animoffer> animoffers;
        animoffers _animoffers;

        /* animal instances - stored within `animals' */
        /// @abi table animals i64
        struct animal {
            uint64_t id;

            // organisation that offered this animal
            account_name org;
            account_name owner;

            // e.g. rattus rattus for rat
            std::string species_name;
            // e.g. brown rat
            std::string common_name;
            // user assigned name. can't be changed except for once!
            // this is initially empty, and can be changed only when empty.
            // max length of 15
            std::string assigned_name;

            /* Clamp these to 1000 */
            uint16_t belly = 500; // hunger level: 1000 is full, 0 is starving.
            uint16_t happiness = 500; // 1000 is ecstatic, 0 is roman bathing
            uint16_t satisfaction = compute_satisfaction(happiness, belly);

            uint32_t last_fed;
            uint16_t last_belly;

            // number of seconds in a week: 7 * 24 * 60 * 60 = 604800
            // So we want the belly to reduce one unit every 605 seconds for the animal to be starving completely after a week.
            uint16_t decay_rate = 605;

            // currently possessed items
            std::vector<item_id> items;
            // history of interactions of with the animal 
            std::vector<std::string> history;

            auto primary_key() const { return id; }
            account_name by_owner() const { return owner; }
            EOSLIB_SERIALIZE( animal, (id)(org)(owner)(species_name)(common_name) \
                    (assigned_name) \
                    (belly)(happiness)(satisfaction)\
                    (last_fed)(last_belly)\
                    (decay_rate)(items)(history) )
        };

        typedef eosio::multi_index<N(animals), animal> animals;
        //typedef eosio::multi_index<N(animals), animal, 
        //                           indexed_by<N(byowner), const_mem_fun<animal, 
        //                                      account_name, &animal::by_owner>>
        //                          > animals;
        animals _animals;

        /** item that can interact with an animal - stored within `items'
         * Items:
         *  * Hat; increase happiness, non-consumable
         *  * Brush; increase happiness, consumable
         *  * House; increase happiness, non-consumable
         *  * Bread; increase fullness, consumable
         **/
        /// @abi table items i64
        struct item {
            item_id id;
            account_name org;
            std::string name; 

            std::string type;
            int16_t hap_delta;
            int16_t hung_delta;

            auto primary_key() const { return id; }
            account_name by_org() const { return org; }

            EOSLIB_SERIALIZE( item, (id)(org)(name)(type)(hap_delta)(hung_delta)) 
        };

        typedef eosio::multi_index<N(items), item, 
                indexed_by<N(byorg), const_mem_fun<item, account_name, 
                &item::by_org>>> items;
        items _items;

        /* represents a user's balance and list of obtained items - stored in `users' */
        /// @abi table users i64
        struct user {
            account_name owner;

            std::vector<item_id> items;

            // XXX: fake currency
            uint64_t balance = 80000;

            auto primary_key() const { return owner; }
            EOSLIB_SERIALIZE( user, (owner)(items)(balance)) 
        };

        typedef eosio::multi_index<N(users), user> users;
        users _users;
};
