#include "eospets.hpp"

bool eospets::anim_owned_by(const uint64_t& animid, const account_name& owner) {
    // animal exists
    auto animit = _animals.find(animid);
    if (animit == _animals.end()) return false;

    // user exists
    auto userit = _users.find(owner);
    if (userit == _users.end()) return false;

    // and if the animal actually is owned by
    return animit->owner == owner;
}

void eospets::assert_org(const account_name& org) {
    eosio_assert(_orgs.find(org) != _orgs.end(), "organisation doesn't exist");
}

void eospets::assert_item(const item_id& itemid) {
    eosio_assert(_items.find(itemid) != _items.end(), "item doesn't exist");
}

void eospets::assert_user(const account_name& username) {
    eosio_assert(_users.find(username) != _users.end(), "user doesn't exist");
}

void eospets::assert_anim(const uint64_t& animid) {
    eosio_assert(_animals.find(animid) != _animals.end(), "animal doesn't exist");
}

void eospets::assert_itemoffer(const uint64_t offerid) {
    eosio_assert(_itemoffers.find(offerid) != _itemoffers.end(), "no such item offer exists.");
}

void eospets::assert_animoffer(const uint64_t offerid) {
    eosio_assert(_animoffers.find(offerid) != _animoffers.end(), "no such animal offer exists");
}

bool eospets::is_orphan(const uint64_t animal_id) {
    assert_anim(animal_id);
    auto animal_it = _animals.find(animal_id);
    return animal_it->owner == _self;
}

uint16_t eospets::clamp(uint16_t val, uint16_t min, uint16_t max) {
    if (val < min) { return min; }
    else if (val > max) { return max; }
    return val;
}

uint16_t eospets::compute_satisfaction(double happiness, double belly) {
    // TODO: Add an 80% threshold for the satisfaction burn.
    return clamp(static_cast<uint16_t>(happiness * (belly / static_cast<double>(MAX_STAT))), 0, MAX_STAT);
}

uint16_t eospets::compute_hunger(const uint32_t& last_fed,
        const uint16_t& last_belly,
        const uint16_t& decay_rate) {
    // Lerp between last fed and now, at decay rate, clamped inside range.
    uint32_t instant = now();
    eosio_assert(last_fed < instant, "Last time fed happened in the future???");

    double seconds_elapsed = instant - last_fed;
    uint16_t hunger_reduction = static_cast<uint16_t>(seconds_elapsed / (double)decay_rate);
    if (hunger_reduction > last_belly) return 0;
    return clamp(last_belly - hunger_reduction, 0, MAX_STAT); 
}

void eospets::makeorg(const account_name& o) {

    require_auth(o);
    eosio_assert(_orgs.find(o) == _orgs.end(), "Organisation already exists.");

    _orgs.emplace(o, [&](auto& r) {
            r.owner = o;
            });
    makeuser(o);
}

void eospets::makeuser(const account_name& u) {
    require_auth(u);

    eosio_assert(_users.find(u) == _users.end(), "User already exists.");
    
    _users.emplace(u, [&](auto& _u) {
            _u.owner = u;
            });
}

void eospets::makeanimal(const account_name& o, std::string species_name,
                         std::string common_name, uint16_t decay_rate) {
    require_auth(o);
    eosio_assert(_orgs.find(o) != _orgs.end(), "User is not an organisation.");

    // make a new animal in the table
    _animals.emplace(o, [&](auto& a) {
            a.id = _animals.available_primary_key();
            a.org = o;
            a.owner = o; // New animals are by default owned by the organisation that created them.
            a.species_name = species_name;
            a.common_name = common_name; 
            a.last_fed = now();
            a.last_belly = a.belly;

            if (decay_rate != 0) { a.decay_rate = decay_rate; }

            // TODO: Return primary key
            // we can't return the newly created id (eos limitation). 
            // so we hack it, and set the mrec animal in the calling
            // organisation that created this animal
            _orgs.modify(_orgs.find(o), o, [&]( auto& orgie ) {
                    orgie.mr_animal = a.id;
                    });
            a.history.push_back(common_name + " born at ::" + std::to_string(now()) + "::.");
            });
}

/// @abi action
void eospets::makeitem(const account_name& orgo, const std::string& name,
                       std::string type, int16_t hap_delta, int16_t hung_delta) {
    require_auth(orgo);
    eosio_assert(_orgs.find(orgo) != _orgs.end(), "User is not an organisation");
    // ensure that our subset of valid item types
    eosio_assert(type == "FOOD" || type == "ACCESSORY" || type == "CONSUMABLE", "please gib a proper item");

    // Logically equivalent to:
    // Item not hunger affecting -> item is not food.
    eosio_assert(hung_delta != 0 || (type != "FOOD"), "Non-food item has hunger effect.");

    // XXX: following comment I believe is out of date.
    // TODO: find if item exists first
    //auto _itemsbyorg = _items.get_index<N(byorg)>();
    //eosio_assert(_itemsbyorg.find(orgo) == _itemsbyorg.end(), "item with same name already exists");

    // instantiate a new item
    _items.emplace(orgo, [&](auto& newitem) {
            newitem.id = _items.available_primary_key();
            newitem.org = orgo;
            newitem.name = name;
            newitem.type = type;
            newitem.hap_delta = hap_delta;
            newitem.hung_delta = hung_delta;

            // TODO: Return primary key
            // ^ again, set most recent item to make up for EOS shortcomings
            _orgs.modify(_orgs.find(orgo), orgo, [&]( auto& orgie) {
                    orgie.mr_item = newitem.id;
                    });
            });
}


void eospets::offeritem(const account_name& orgo, const item_id& itemid, 
        const uint64_t& price) {
    require_auth(orgo);
    // check orgo is an org
    assert_org(orgo);
    // check itemid is an item
    assert_item(itemid);
    // check item of itemid has the org == orgo
    eosio_assert(_items.find(itemid)->org == orgo, "item is not owned by organisation");

    // XXX: retcon following line, anton has said its fine (TM)
    // check there is no already existing offer for this item?

    // create sale
    _itemoffers.emplace(orgo, [&](auto& s) {
            s.id = _itemoffers.available_primary_key();
            s.org = orgo;
            s.itemid = itemid;
            s.price = price;
            }); 
}

void eospets::offeranim(const account_name& orgo, const std::string& species_name,
                       const std::string& common_name, const uint64_t price) {

            require_auth(orgo);
            assert_org(orgo);
            
            // well, that was easy! display the offer
            _animoffers.emplace(orgo, [&](auto& ao) {
                ao.id = _animoffers.available_primary_key();
                
                ao.org = orgo;
                ao.species_name = species_name;
                ao.common_name = common_name;

                ao.price = price;
            });
}

void eospets::buyanim(const account_name& owner, const uint64_t& animoffid){
    require_auth(owner);

    assert_user(owner);
    assert_animoffer(animoffid);

    auto userit = _users.find(owner);
    auto ofit = _animoffers.find(animoffid);

    eosio_assert(userit->balance >= ofit->price, 
            "user has insufficient funds to proceeed");

    _users.modify(userit, owner, [&](auto& u) {
            u.balance -= ofit->price;
            });

    // create an animal with this blueprint
    _animals.emplace(owner, [&](auto& a) {
            a.id = _animals.available_primary_key();
            a.org = ofit->org;
            a.owner = owner;
            a.species_name = ofit->species_name;
            a.common_name = ofit->common_name;
            a.last_fed = now(); 
            a.last_belly = a.belly;
            a.history.push_back(ofit->common_name + " born at ::" + std::to_string(now()) + "::.");

            _orgs.modify(_orgs.find(ofit->org), owner, [&]( auto& orgie ) {
                    orgie.mr_animal = a.id;
                    });
            });
}

void eospets::buyitem(const account_name& buyer, const uint64_t offerid) {
    require_auth(buyer);
    assert_user(buyer);
    assert_itemoffer(offerid);

    auto offer = _itemoffers.find(offerid);
    auto buyer_struct = _users.find(buyer);

    // Check sufficient balance.
    auto price = offer->price;
    eosio_assert(buyer_struct->balance >= price, "Insufficient balance.");

    _users.modify(buyer_struct, buyer, [&](auto& u) {
            u.balance -= price;
            u.items.push_back(offer->itemid);
            });
}

/// @abi action
// an organisation will be able to put an animal in the orphanage
//void offeranimal(const account_name& org, std::string species_name,
//                 std::string common_name, const uint64_t& price, const uint64_t& number) {

//        require_auth(org);                
//        // check org is an org        
//        assert_org(org);
//        // emplace a new offer
//        //_offers
//}

// a test fn to check that we can query by something other than the primary key
void eospets::printitemid(const account_name& owner)  {
    auto itemsbyorg = _items.get_index<N(byorg)>();
    eosio_assert(itemsbyorg.find(owner) != itemsbyorg.end(), "item not found");
    std::string s(std::to_string(itemsbyorg.find(owner)->id));
    print(s);
}

void eospets::setanimname(const account_name& owner, const uint64_t& id, 
        const std::string& new_name) {
    require_auth(owner);

    eosio_assert(new_name.size() <= 15, "supplied name too long");
    auto fnd = _animals.find(id);
    // perform some checks to see if the user can change its name
    eosio_assert(anim_owned_by(id, owner), "animal/user doesn't exist or isn't owned");
    eosio_assert(fnd->assigned_name == "", "animal already has a name. You monster.");

    // actually change it!
    _animals.modify(_animals.find(id), owner, [&]( auto& anim ) {
            anim.assigned_name = new_name;
            });
}

/** 
 * Apply an item to an animal, removing it if consumable   
 */
void eospets::useitem(const account_name& owner, const item_id& itemid,
        const uint64_t& animalid) {
    require_auth(owner);
    // ensure that animal exists
    eosio_assert(anim_owned_by(animalid, owner), "animal/user doesn't exist or isn't owned by");

    // have an iterator for each the item and animal
    auto itemit = _items.find(itemid);
    auto animit = _animals.find(animalid);

    eosio_assert(itemit != _items.end(), "item doesn't exist");
    // organisation item can only be used on animal org
    eosio_assert(itemit->org == animit->org, "item used on animal not owned by same organisation");

    auto userit = _users.find(owner);
    // this is guaranteed not to fail, but w/e.
    eosio_assert(userit != _users.end(), "user not exists");

    // check the user owns the item
    const std::vector<item_id>& owned_items = userit->items;
    auto founditem = std::find_if(owned_items.cbegin(), owned_items.cend(), [&](const item_id& i) {
            return i == itemid;
            });
    eosio_assert(founditem != owned_items.end(), "user does not own item");

    // Recompute the animal's hunger/satisfaction due to linear decay etc.
    update(owner, animalid);

    // If the animal's hunger is 0 then they ran away already, we can just return.
    if (animit->belly == 0) return;

    // Update happiness and hunger.
    uint16_t new_hung = clamp(animit->belly + itemit->hung_delta,
            0, MAX_STAT);
    uint16_t new_hap = clamp(animit->happiness + itemit->hap_delta,
            0, MAX_STAT);
    _animals.modify(animit, owner, [&](auto& a) {
            a.belly = new_hung;
            a.happiness = new_hap;
            });

    // Recompute satisfaction now that the animal has been fed.
    update(owner, animalid);

    // Consume items.
    // all items are removed from user account
    _users.modify(userit, owner, [&](auto& u) {
            u.items.erase(founditem);
            });

    // Transfer items and log.
    if (itemit->type == "CONSUMABLE") {
        addlog(owner, animalid, itemit->name + " was used.");
    } else if (itemit->type == "FOOD") {
        _animals.modify(animit, owner, [&](auto& a) {
                a.last_fed = now();
                a.last_belly = a.belly;
                }); 
        addlog(owner, animalid, itemit->name + " was eaten.");
    } else if (itemit->type == "ACCESSORY") {
        // if non-consumable and nonfood, the animal owns them
        _animals.modify(animit, owner, [&](auto& a) {
                a.items.push_back(itemid);
                });
        addlog(owner, animalid, itemit->name + " was given.");
    } else {
        eosio_assert(false, "fuck you, no such item type");
    }
}

void eospets::transfer(const account_name& fromacc, const account_name& toacc,
        const uint64_t& animid) {
    require_auth(fromacc);
    // ensure that the two users exist
    eosio_assert(_users.find(fromacc) != _users.end(), "sender doesn't exist");
    eosio_assert(_users.find(toacc) != _users.end(), "receiver doesn't exist");
    auto animit = _animals.find(animid);
    eosio_assert(animit != _animals.end(), "animal doesn't exist");
    eosio_assert(animit->owner == fromacc, "animal isn't owned by sender"); 

    // audit string to put in our animal
    // TODO: Make these the proper base32 encoded strings
    std::string xfer_str = "Transfer " + name{fromacc}.to_string() + " -> " + name{toacc}.to_string();

    _animals.modify(animit, fromacc, [&](auto& anim) {
            anim.owner = toacc;
            anim.history.push_back(xfer_str);
            });
}

void eospets::abandonanim( const account_name& owner, const uint64_t& animid) {
    require_auth(owner);
    assert_user(owner);
    assert_anim(animid);

    eosio_assert(anim_owned_by(animid, owner), "animal isn't owned by owner");

    _animals.modify(_animals.find(owner), owner, [&]( auto& a ) {
            a.owner = _self;
            std::string xfer_str = "Sent animal to the pound voluntarily.";
            a.history.push_back(xfer_str);
            });
}

void eospets::addlog(const account_name& o, const uint64_t animid, const std::string& log) {
    require_auth(o);
    eosio_assert(_orgs.find(o) != _orgs.end(), "User is not an organisation.");
    auto anim_iter = _animals.find(animid);
    eosio_assert(anim_iter != _animals.end(), "Animal does not exist.");
    eosio_assert(anim_iter->org == o, "Animal does not belong to given organisation.");
    _animals.modify(anim_iter, o, [&](auto& anim) {
            anim.history.push_back(log);
            });
}

void eospets::update(const account_name& payer, const uint64_t& animal_id) {
    assert_anim(animal_id);
    auto animal_it = _animals.find(animal_id);

    // Recompute hunger according to linear decay.
    uint16_t new_belly = compute_hunger(animal_it->last_fed,
            animal_it->last_belly,
            animal_it->decay_rate);

    // Recompute satisfaction according to recomputed hunger.
    uint16_t new_satisfaction = compute_satisfaction(animal_it->happiness,
            new_belly);

    // Actually update the animal
    _animals.modify(animal_it, payer, [&](auto& anim) {
            anim.belly = new_belly; 
            anim.satisfaction = new_satisfaction;
            });

    // If hunger falls to 0, animal runs away if it has not already.
    if (new_belly == 0 && !is_orphan(animal_id)) {
        auto org = animal_it->org;
        assert_org(org);

        _animals.modify(animal_it, payer, [&](auto& anim) {
                anim.owner = _self; 
                });   
        addlog(org, animal_id, "Starved and ran away.");
    }

}


EOSIO_ABI( eospets, (makeorg)(makeuser)(makeanimal)(makeitem)(offeritem)(buyitem)(offeranim)(buyanim)(printitemid)(setanimname)(useitem)(transfer)(addlog)(update)(sendmoney) )

