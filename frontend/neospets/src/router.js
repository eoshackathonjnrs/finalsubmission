import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode  : 'history',
  base  : process.env.BASE_URL,
  routes: [
    {
      path     : '/',
      name     : 'home',
      component: Home
    },

    {
      path     : '/organisations/:owner',
      name     : 'organisations-single',
      component: () => import(/* webpackChunkName: "org-single" */ './views/OrganisationsSingle.vue'),
    },

    {
      path     : '/organisations',
      name     : 'organisations',
      component: () => import(/* webpackChunkName: "orgs" */ './views/Organisations.vue')
    },

    {
      path     : '/pets/:id',
      name     : 'pets-single',
      props    : true,
      component: () => import(/* webpackChunkName: "post-single" */ './views/PetsSingle.vue'),
    },

    {
      path     : '/pets',
      name     : 'pets',
      component: () => import(/* webpackChunkName: "pets" */ './views/Pets.vue')
    },

    {
      path     : '/marketplace',
      name     : 'marketplace',
      component: () => import(/* webpackChunkName: "orphanage" */ './views/Marketplace.vue')
    },


    {
      path     : '/shop',
      name     : 'shop',
      component: () => import(/* webpackChunkName: "shop" */ './views/Shop.vue')
    },


    {
      path     : '/admin',
      name     : 'admin',
      component: () => import(/* webpackChunkName: "admin" */ './views/Admin.vue')
    },

  ]
})
