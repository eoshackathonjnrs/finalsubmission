import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false


import _ from 'lodash'
Vue.filter('titleCase', value => {
  if (!value){
    return ''
  }

  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
