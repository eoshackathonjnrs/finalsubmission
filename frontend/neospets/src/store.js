import Vue from 'vue'
import Vuex from 'vuex'
import randomColor from 'randomcolor';

import * as EosData from './eos';

Vue.use(Vuex);


const animal_map = {
  dugong: 'dugong',
  numbat: 'numbat',
  albatross: 'albatross',
  cockatoo: 'blackcockatoo',
    ibis: 'ibis',
    koala: 'koala',
};

const item_map = {
  corn: 'corn'
}


const org_map = {
    anton: {
        img_file: 'bin.jpg',
        desc_html: 'Anton is a individual in his community who wants to do something good for his local parks, and is starting initiatives to clean the streets.'
    },
    wwf: {
        img_file: 'WWF_logo.svg',
        desc_html: 'We’re building a future in which people live in harmony with nature.'
    },
    tarongazoo: {
        img_file: 'taronga_logo.png',
        desc_html: 'At Taronga, we believe that wildlife and people can share this planet.'
    }
};

export default new Vuex.Store({

  // Pure raw data
  state: {
    users         : [],
    items         : [],
    animals       : [],
    organisations : [],
    market_items  : [],
    market_animals: [],
  },

  // Computed data here
  getters: {



    items(state) {
      return state.items.map(item => {

        return {...item}
      })
    },

    users(state) {
      return state.users.map(u => {
        const items = u.items.map(id => state.items.find(item => item.id === id))
        return {...u, items}
      })
    },

    animals(state) {
      return state.animals.map(a => {
        const items = a.items.map(id => state.items.find(item => item.id === id));


        const statuses = [
          {name: 'Fullness', value: a.belly, max: 1000},
          {name: 'Happiness', value: a.happiness, max: 1000},
          {name: 'Satisfaction', value: a.satisfaction, max: 1000}
        ];


        const bg_color_1 = randomColor({luminosity: 'light', seed: a.id})
        const bg_color_2 = randomColor({luminosity: 'light', seed: a.id + 10})
        return {...a, items, bg_color_1, bg_color_2, image: animal_map[a.common_name], statuses}
      })
    },

    market_animals(state) {
      return state.market_animals.map(x => {
        return {
          ...x,
          bg_color_1: randomColor({luminosity: 'light', seed: x.id}),
          bg_color_2: randomColor({luminosity: 'light', seed: x.id + 10}),
          image     : animal_map[x.common_name],
        }
      })
    },
    market_items(state) {
      return state.market_items.map(market_item => {

        const item = state.items.find(x => x.id === market_item.itemid)

        const image = item_map[item.name];

        return {
          ...market_item, item, image
        }
      })
    },


    organisations(state) {


      return state.organisations.map(a => {
        let owner = a.owner;
        const animals = state.animals.filter(x => x.org === owner);

        let desc_html = '';
        let img_file = '';
        if(org_map[owner]){
          desc_html = org_map[owner].desc_html;
          img_file = org_map[owner].img_file;
        }

        return {...a, desc_html, img_file, animals};
      })
    }
  },


  // Atomic state changes here
  mutations: {
    loadUsers(state, {users}) {
      state.users = users;
    },

    loadItems(state, {items}) {
      state.items = items;
    },

    loadAnimals(state, {animals}) {
      state.animals = [...animals];
    },

    loadOrganisations(state, {organisations}) {
      state.organisations = organisations;
    },


    loadAnimalOffers(state, {offers}) {
      state.market_animals = offers
    },

    loadItemOffers(state, {offers}) {
      state.market_items = offers
    }
  },

  // Async or composed mutations go here
  actions: {
    fetchAnimals({commit}) {
      return EosData.getAnimals()
        .then(animals => commit('loadAnimals', {animals}))
    },


    fetchUsers({commit}) {
      return EosData.getUsers()
        .then(users => commit('loadUsers', {users}))
    },


    // TODO: FIRST WE NEED USERS, ESP ONE THAT IS LOGGED IN AND NOT ANTON
    // THEN GET THE ITEMS THE USER OWNS BEFORE DOING THIS SHIT
    useItem({commit, dispatch}, {animal_id, item_id}) {
      return EosData.useItem('anton', 'anton', item_id, animal_id)
        .then(() => {
          return dispatch('fetchAnimals')
        })
    }
  }
})
