const {
        chainId,
        httpEndpoint,
        keyProvider,
        expireInSeconds,
        sign,
        verbose,
      } = require('../eosconfig');


import Eos from 'eosjs';


export const eos          = Eos({httpEndpoint, chainId, keyProvider,expireInSeconds, sign, verbose});

export const getTableData = table => eos.getTableRows({
  json : true,
  code : 'eospets',
  scope: 'eospets',
  table,
  limit: 100
}).then(({rows}) => rows);





export const getAnimals = () => getTableData('animals');
export const getAnimalOffers = () => getTableData('animoffers');
export const getUsers = () => getTableData('users');
export const getOrganisations = () => getTableData('orgs');
export const getItems = () => getTableData('items');
export const getMarketItems = () => getTableData('itemoffers');



export const makeAnimal = (account_name, species_name, common_name ) => {
  return eos.contract("eospets")
    .then(user => {
      return user.makeanimal(account_name, species_name, common_name, {authorization: account_name})
    })
}

// type must be FOOD, ACCESSORY, CONSUMABLE
export const makeItem = (account_name, org, name, type, hap_delta = 0, hung_delta = 100) => {
  return eos.contract("eospets").then(user => {
    return user.makeitem(org, name, type, hap_delta, hung_delta, {authorization: account_name})
  })
}

export const makeOffer = (account_name, org, item_id, price = 0) => {
  return eos.contract("eospets").then(user => {
    return user.offeritem(org, item_id, price, {authorization: account_name})
  })
}

export const offerAnim = (org, species_name, common_name, price) => {
    return eos.contract("eospets").then(user => { 
        return user.offeranim(org, species_name, common_name, price, {authorization: org});
    });
}

export const buyItem = (account_name, buyer, offer_id) => {
  return eos.contract('eospets').then(user => {
    return user.buyitem(buyer, offer_id, {authorization: account_name})
  })
}



export const updateAnimalName = (account_name, owner, animal_id, name) => {
  return eos.contract('eospets')
    .then(user => {
      return user.setanimname(owner, animal_id, name, {authorization: account_name})
    })
};

export const useItem = (account_name,owner,item_id,animal_id) => {
  return eos.contract('eospets')
    .then(user => {
      return user.useitem(owner, item_id, animal_id, {authorization:account_name})
    })
}

export const buyAnim = (anim_offer_id) => {
  return eos.contract('eospets')
    .then(user => {
      return user.buyanim('anton', anim_offer_id, {authorization:'anton'})
    })
}


export const transferPet = (to, id) => {
  return eos.contract('eospets')
    .then(user => {
      return user.transfer('anton', to, id, {authorization:'anton'});
    })
}


export const releasePet = (id) => {
  return eos.contract('eospets')
    .then(user => {
      return user.transfer('anton', 'eospets', id, {authorization:'anton'});
    })
}

